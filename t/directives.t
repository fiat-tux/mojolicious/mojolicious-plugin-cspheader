use Mojo::Base -strict;

use Test::More;
use Mojolicious::Lite;
use Test::Mojo;

plugin 'CSPHeader', directives => {
    'default-src' => "'none'",
    'font-src'    => "'self'",
    'img-src'     => "'self' data:",
    'style-src'   => "'self'",
    'connect-src' => {
        base => "'self'",
        ws   => 1
    }
};

get '/' => sub {
  my $c = shift;
  $c->render(text => 'Hello Mojo!');
};

my $t = Test::Mojo->new;
$t->get_ok('/' => { Host => 'test.org' })
  ->status_is(200)
  ->header_is('Content-Security-Policy' => "connect-src 'self' ws://test.org; default-src 'none'; font-src 'self'; img-src 'self' data:; style-src 'self'")
  ->content_is('Hello Mojo!');

$t = $t->get_ok('/something.js' => { Host => 'test.org' })
  ->status_is(200)
  ->header_is('Content-Security-Policy' => "connect-src 'self' ws://test.org; default-src 'none'; font-src 'self'; img-src 'self' data:; style-src 'self'")
  ->content_is("alert('hello!');\n");

done_testing();
